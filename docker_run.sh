#!/usr/bin/env bash

AMPQ_URL="amqp://guest:guest@localhost:5672/"

docker build . -t eventuate-message-relay

# Use host network for testing only
docker run --net=host --rm -e AMPQ_URL=$AMPQ_URL eventuate-message-relay
