package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/google/go-cmp/cmp"
	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
)

type Message struct {
	Service     string `json:"service,omitempty"`
	RoomID      string `json:"roomid,omitempty"`
	MessageText string `json:"messagetext,omitempty"`
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func EnqueMessage(message Message) {
	conn, err := amqp.Dial(os.Getenv("AMPQ_URL"))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		message.Service, // name
		false,           // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare a queue")

	body, err := json.Marshal(message)
	failOnError(err, "Failed to marshall message to json")

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
	failOnError(err, "Failed to publish a message")
}

func GetMessage(w http.ResponseWriter, r *http.Request) {
	var message Message

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &message); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessablle entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}
	if cmp.Equal(message, Message{}) {
		err := "JSON is incorrect"
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		log.Println("Encounters error", err)
		return
	}
	log.Println("RoomID:", message.RoomID)
	log.Println("Message Text:", message.MessageText)
	EnqueMessage(message)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/api/v1/messages/", GetMessage).Methods("POST")
	log.Fatal(http.ListenAndServe(":8001", router))
}
