FROM golang:1.10-alpine

WORKDIR /go/src/app

RUN apk update && apk add ca-certificates && \
    apk add git && rm -rf /var/cache/apk/*

RUN go get github.com/gorilla/mux && \
    go get github.com/google/go-cmp/cmp && \
    go get github.com/streadway/amqp

COPY src .
RUN go build -o main . 

CMD ["/go/src/app/main"]

